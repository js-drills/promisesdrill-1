const fs = require("fs").promises;
const path = require("path");

const filepath = path.join(__dirname, "lipsum_1.txt");

function readingFile(filename) {
  return fs.readFile(filename, "utf-8");
}

function creatingDirectoryIfNotExists(directory) {
  return fs.access(directory, fs.constants.F_OK).catch((error) => {
    if (error.code === "ENOENT") {
      return fs.mkdir(directory);
    }
    throw error;
  });
}

function convertingToUpperCase(data, directory) {
  const upperCase = data.toUpperCase();
  const filename = "UpperCaseData.txt";
  const filepath = path.join(directory, filename);
  return fs
    .writeFile(filepath, upperCase)
    .then(() => {
      console.log("Uppercase File Generated");
      return appendNamesToFile(filepath, "filenames.txt");
    })
    .then(() => {
      return { directory, filename };
    });
}

function convertingToSentences(directory, filename) {
  return fs
    .readFile(path.join(directory, filename), "utf-8")
    .then((upperCaseData) => {
      const toLowerCaseData = upperCaseData.toLowerCase();
      const sentences = toLowerCaseData.split(/[.!?]/);
      const modifiedContent = sentences.join("\n");
      return fs
        .writeFile(path.join(directory, "splittedContent.txt"), modifiedContent)
        .then(() => {
          console.log("Split sentences file written successfully");
          return appendNamesToFile(
            path.join(directory, "splittedContent.txt"),
            "filenames.txt"
          );
        })
        .then(() => {
          return directory;
        });
    });
}

function sortingFiles(directory) {
  const fileNamesPath = path.join(directory, "filenames.txt");
  return fs.readFile(fileNamesPath, "utf-8").then((filesnames) => {
    const files = filesnames.split("\n");
    const sortedData = files
      .slice(0, 2)
      .map((file) => {
        return fs.readFile(file, "utf-8");
      })
      .sort()
      .join("\n");
    const sortedfilePath = path.join(directory, "sortedData.txt");
    return fs
      .writeFile(sortedfilePath, sortedData)
      .then(() => {
        console.log("Sorted files has been written successfully");
        return appendNamesToFile(sortedfilePath, "filenames.txt");
      })
      .then(() => {
        return files;
      });
  });
}

function appendNamesToFile(filenameToAdd, targetFilename) {
  const filename = `../problem2files/${targetFilename}`;
  return fs.readFile(filename, "utf-8").then((data) => {
    if (!data.includes(filenameToAdd)) {
      return fs.appendFile(filename, filenameToAdd + "\n").then(() => {
        console.log(`${filenameToAdd} added to ${targetFilename}`);
      });
    }
  });
}

function deleteFiles(files) {
  if (files.length === 0) {
    console.log("No files to delete.");
    return Promise.resolve();
  }
  return Promise.all(
    files.slice(0, -1).map((file) => {
      console.log(`${file} is deleted`);
      return fs.unlink(file).catch((error) => {
        console.error(`Error deleting file ${file}: ${error}`);
      });
    })
  ).then(() => {
    console.log("All files deleted successfully.");
  });
}

async function finalCalls() {
  const filename = "../lipsum_1.txt";
  try {
    const data = await readingFile(filename);
    const directory = "../problem2files";
    await creatingDirectoryIfNotExists(directory);
    const { directory: convertedDirectory, filename: convertedFilename } =
      await convertingToUpperCase(data, directory);
    const sentencesDirectory = await convertingToSentences(
      convertedDirectory,
      convertedFilename
    );
    const files = await sortingFiles(sentencesDirectory);
    await deleteFiles(files);
  } catch (error) {
    console.error(`Error occurred: ${error}`);
  }
}

module.exports = finalCalls;


