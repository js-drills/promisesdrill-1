// const fs = require("fs").promises;
// const path = require("path");

// function createRandomJSONFiles(directory, numFiles) {
//   return fs
//     .access(directory, fs.constants.F_OK)
//     .then(() => {
//       console.log(`Directory ${directory} exists`);
//     })
//     .catch((err) => {
//       if (err.code === "ENOENT") {
//         // Directory doesn't exist, create it
//         return fs.mkdir(directory).then(() => {
//           console.log(`Directory ${directory} created`);
//         });
//       } else {
//         // Other error, rethrow it
//         throw err;
//       }
//     })
//     .then(() => {
//       // Directory exists or was created, proceed to create files
//       const files = Array.from({ length: numFiles }, (_, i) =>
//         path.join(directory, `file${i}.json`)
//       );
//       return Promise.all(
//         files.map((file) => {
//           const data = JSON.stringify({ message: "Random data" });
//           return fs.writeFile(file, data).then(() => {
//             console.log(`File ${file} created`);
//             return file;
//           });
//         })
//       );
//     });
// }

// function deleteFiles(files) {
//   return Promise.all(
//     files.map((file) => {
//       return fs.unlink(file).then(() => {
//         console.log(`File ${file} deleted`);
//       });
//     })
//   );
// }

let fs = require("fs/promises");
const path = require("path");

let createRandomJSONFiles = (directry, numFiles) => {
  return fs
    .access(directry, fs.constants.F_OK)
    .then(() => {
      console.log(`Directory named ${directry} exits`);
    })
    .catch((error) => {
      if (error.code === "ENOENT") {
        return fs.mkdir(directry).then(() => {
          console.log(`Directory named ${directry} is created Successfully`);
        });
      } else {
        throw error;
      }
    })
    .then(() => {
      const files = Array.from({ length: numFiles }, (_, i) => {
        return path.join(directry, `file${i}.json`);
      });
      return Promise.all(
        files.map((file) => {
          const data = JSON.stringify({ message: "RandomData" });
          return fs.writeFile(file, data).then(() => {
            console.log(`File names ${file} is written successfully`);
            return file;
          });
        })
      );
    });
};

function deleteFiles(files) {
  return Promise.all(
    files.map((file) => {
      return fs.unlink(file).then(() => {
        console.log(`\n\n The file named ${file} has beeen deleted`);
      });
    })
  );
}
module.exports = { createRandomJSONFiles, deleteFiles };
