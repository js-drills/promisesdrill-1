const functions = require("../problem1");
const createRandomJSONFiles = functions.createRandomJSONFiles;
const deleteFiles = functions.deleteFiles;

const directory = "./randomJsonFiles";
const numFiles = 5;

async function excutingFunction() {
  // console.log("running");
  try {
    let files = await createRandomJSONFiles(directory, numFiles);
    await deleteFiles(files);
    console.log("\n\n All Files Deleted Succesfully");
  } catch (error) {
    console.log(`Error araised: ${error}`);
  }
}
excutingFunction();
